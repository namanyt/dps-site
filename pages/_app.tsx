import "../styles/globals.css";
import type { AppProps } from "next/app";

import { store } from "../service/store";
import { Provider as ReduxProvider } from "react-redux";
import { MantineProvider } from "@mantine/core";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ReduxProvider store={store}>
      <MantineProvider
        withNormalizeCSS
        withCSSVariables
        theme={{ colorScheme: "dark" }}
      >
        <Component {...pageProps} />
      </MantineProvider>
    </ReduxProvider>
  );
}
