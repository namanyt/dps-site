import {
  createStyles,
  Header,
  HoverCard,
  Group,
  Button,
  UnstyledButton,
  Text,
  SimpleGrid,
  ThemeIcon,
  Anchor,
  Divider,
  Center,
  Box,
  Burger,
  Drawer,
  Collapse,
  ScrollArea,
  Image,
} from "@mantine/core";
import { MantineLogo } from "@mantine/ds";
import { useDisclosure } from "@mantine/hooks";
import {
  IconNotification,
  IconCode,
  IconBook,
  IconChartPie3,
  IconFingerprint,
  IconCoin,
  IconChevronDown,
  IconSchool,
  IconBuilding,
  IconBooks,
  IconFileCertificate,
} from "@tabler/icons";

import { LOGIN } from "../types/const";

const useStyles = createStyles((theme) => ({
  link: {
    display: "flex",
    alignItems: "center",
    height: "100%",
    paddingLeft: theme.spacing.md,
    paddingRight: theme.spacing.md,
    textDecoration: "none",
    color: theme.colorScheme === "dark" ? theme.white : theme.black,
    fontWeight: 500,
    fontSize: theme.fontSizes.sm,
    cursor: "pointer",

    [theme.fn.smallerThan("sm")]: {
      height: 42,
      display: "flex",
      alignItems: "center",
      width: "100%",
    },

    ...theme.fn.hover({
      backgroundColor:
        theme.colorScheme === "dark"
          ? theme.colors.dark[6]
          : theme.colors.gray[0],
    }),
  },

  subLink: {
    width: "100%",
    padding: `${theme.spacing.xs}px ${theme.spacing.md}px`,
    borderRadius: theme.radius.md,

    ...theme.fn.hover({
      backgroundColor:
        theme.colorScheme === "dark"
          ? theme.colors.dark[7]
          : theme.colors.gray[0],
    }),

    "&:active": theme.activeStyles,
  },

  dropdownFooter: {
    backgroundColor:
      theme.colorScheme === "dark"
        ? theme.colors.dark[7]
        : theme.colors.gray[0],
    margin: -theme.spacing.md,
    marginTop: theme.spacing.sm,
    padding: `${theme.spacing.md}px ${theme.spacing.md * 2}px`,
    paddingBottom: theme.spacing.xl,
    borderTop: `1px solid ${
      theme.colorScheme === "dark" ? theme.colors.dark[5] : theme.colors.gray[1]
    }`,
  },

  hiddenMobile: {
    [theme.fn.smallerThan("sm")]: {
      display: "none",
    },
  },

  hiddenDesktop: {
    [theme.fn.largerThan("sm")]: {
      display: "none",
    },
  },
}));

const home_data = [
  {
    icon: IconSchool,
    title: "DPS Jaipur",
    description: "About Us, Contacts, Location, etc.",
    link: "/#about",
  },
  {
    icon: IconBuilding,
    title: "Infrastucture",
    description: "School Building, Labs, Library, Hostels etc.",
    link: "/#infrastructure",
  },
  {
    icon: IconFileCertificate,
    title: "Admission",
    description:
      "Addmission Infomation, Withdrawal, Registration forms, Fee structures, etc.",
    link: "/#admission",
  },
  {
    icon: IconBooks,
    title: "Academics",
    description: "Examinations, Syllabus, Houses, Hall of Fame, etc.",
    link: "/#academics",
  },
];

const result = [
  {
    text: "Class 10th",
    link: "/results/class-10",
  },
  {
    text: "Class 12th",
    link: "/results/class-12",
  },
];

export function HeaderMegaMenu() {
  const [drawerOpened, { toggle: toggleDrawer, close: closeDrawer }] =
    useDisclosure(false);

  const [homeOpened, { toggle: toggleHome }] = useDisclosure(false);
  const [resultsOpened, { toggle: toggleResults }] = useDisclosure(false);

  const { classes, theme } = useStyles();

  const home_links = home_data.map((item) => (
    <UnstyledButton className={classes.subLink} key={item.title}>
      <Group noWrap align="flex-start">
        <ThemeIcon size={34} variant="default" radius="md">
          <item.icon size={22} color={theme.fn.primaryColor()} />
        </ThemeIcon>
        <Anchor href={item.link} color="dimmed">
          <Text size="sm" weight={500}>
            {item.title}
          </Text>
          <Text size="xs" color="dimmed">
            {item.description}
          </Text>
        </Anchor>
      </Group>
    </UnstyledButton>
  ));

  const result_links = result.map((item) => (
    <UnstyledButton className={classes.subLink} key={item.text}>
      <Group noWrap align="flex-start">
        <Anchor color={"dimmed"} href={item.link}>
          <Text size="sm" weight={500}>
            {item.text}
          </Text>
        </Anchor>
      </Group>
    </UnstyledButton>
  ));

  return (
    <Box>
      <Header height={80} px="md">
        <Group position="apart" sx={{ height: "100%" }}>
          <Image src="/images/logo.png" width={"250px"} />

          <Group
            sx={{ height: "75%" }}
            spacing={9}
            className={classes.hiddenMobile}
          >
            <HoverCard
              width={500}
              position="bottom"
              radius="md"
              shadow="md"
              withinPortal
            >
              <HoverCard.Target>
                <span className={classes.link}>
                  <Center inline>
                    <Box component="span" mr={5}>
                      Home
                    </Box>
                    <IconChevronDown
                      size={16}
                      color={theme.fn.primaryColor()}
                    />
                  </Center>
                </span>
              </HoverCard.Target>

              <HoverCard.Dropdown sx={{ overflow: "hidden" }}>
                <Divider
                  style={{ borderTopWidth: 10 }}
                  color={theme.colorScheme === "dark" ? "dark.5" : "gray.1"}
                />

                <SimpleGrid cols={2} spacing={5}>
                  {home_links}
                </SimpleGrid>

                <Divider
                  style={{ borderTopWidth: 10 }}
                  color={theme.colorScheme === "dark" ? "dark.5" : "gray.1"}
                />
              </HoverCard.Dropdown>
            </HoverCard>
            <a href="/gallery" className={classes.link}>
              Gallery
            </a>
            <HoverCard
              width={300}
              position="bottom"
              radius="md"
              shadow="md"
              withinPortal
            >
              <HoverCard.Target>
                <span className={classes.link}>
                  <Center inline>
                    <Box component="span" mr={5}>
                      Results
                    </Box>
                    <IconChevronDown
                      size={16}
                      color={theme.fn.primaryColor()}
                    />
                  </Center>
                </span>
              </HoverCard.Target>

              <HoverCard.Dropdown sx={{ overflow: "hidden" }}>
                <Divider
                  style={{ borderTopWidth: 10 }}
                  color={theme.colorScheme === "dark" ? "dark.5" : "gray.1"}
                />

                <SimpleGrid cols={2} spacing={5}>
                  {result_links}
                </SimpleGrid>

                <Divider
                  style={{ borderTopWidth: 10 }}
                  color={theme.colorScheme === "dark" ? "dark.5" : "gray.1"}
                />
              </HoverCard.Dropdown>
            </HoverCard>
          </Group>

          <Group className={classes.hiddenMobile}>
            <Button
              onClick={() => {
                window.open(LOGIN.Teacher);
              }}
              variant="default"
            >
              Teacher Login
            </Button>
            <Button
              onClick={() => {
                window.open(LOGIN.Student);
              }}
            >
              Student Login
            </Button>
          </Group>

          <Burger
            opened={drawerOpened}
            onClick={toggleDrawer}
            className={classes.hiddenDesktop}
          />
        </Group>
      </Header>

      <Drawer
        opened={drawerOpened}
        onClose={closeDrawer}
        size="100%"
        padding="md"
        title="Navigation"
        className={classes.hiddenDesktop}
        zIndex={1000000}
      >
        <ScrollArea sx={{ height: "calc(100vh - 60px)" }} mx="-md">
          <Divider
            my="sm"
            color={theme.colorScheme === "dark" ? "dark.5" : "gray.1"}
          />

          <UnstyledButton className={classes.link} onClick={toggleHome}>
            <Center inline>
              <Box component="span" mr={5}>
                Home
              </Box>
              <IconChevronDown size={16} color={theme.fn.primaryColor()} />
            </Center>
          </UnstyledButton>
          <Collapse in={homeOpened}>{home_links}</Collapse>
          <a href="/gallery" className={classes.link}>
            Gallery
          </a>
          <UnstyledButton className={classes.link} onClick={toggleResults}>
            <Center inline>
              <Box component="span" mr={5}>
                Results
              </Box>
              <IconChevronDown size={16} color={theme.fn.primaryColor()} />
            </Center>
          </UnstyledButton>
          <Collapse in={resultsOpened}>{result_links}</Collapse>

          <Divider
            my="sm"
            color={theme.colorScheme === "dark" ? "dark.5" : "gray.1"}
          />

          <Group position="center" grow pb="xl" px="md">
            <Button
              onClick={() => {
                window.open(LOGIN.Teacher);
              }}
              variant="default"
            >
              Teacher Login
            </Button>
            <Button
              onClick={() => {
                window.open(LOGIN.Student);
              }}
            >
              Student Login
            </Button>
          </Group>
        </ScrollArea>
      </Drawer>
    </Box>
  );
}
